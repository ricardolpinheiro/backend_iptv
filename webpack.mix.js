const mix = require('laravel-mix');
const ImageminPlugin = require('imagemin-webpack-plugin').default;
const CopyWebpackPlugin = require('copy-webpack-plugin');
const imageminMozjpeg = require('imagemin-mozjpeg');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.s
 |
 */

mix
    /* CSS */
    .sass('resources/sass/main.scss', 'public/css/app.css')

    /* JS */
    .js('resources/js/app.js', 'public/js/laravel.app.js')
    .autoload({
        jquery: ['$', 'window.jQuery', 'jQuery'],
    })

    /* Tools */
    .browserSync('localhost:8000')
    .disableNotifications()

    /* Options */
    .options({
        processCssUrls: false
    });


mix.webpackConfig({
    plugins: [
        new CopyWebpackPlugin([{
            from: 'storage/app/public/original',
            to: 'storage/w80', // Laravel mix will place this in 'public/img'
        }]),
        new ImageminPlugin({
            test: /\.(jpe?g|png|gif|svg)$/i,
            plugins: [
                imageminMozjpeg({
                    quality: 80,
                })
            ]
        })
    ]
});
